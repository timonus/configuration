import sys
import subprocess
import re
import os
import shutil
import json

if len(sys.argv) < 2:
    print "No path specified"
    sys.exit()
    
path = sys.argv[1]

data = json.loads(open(path).read())
data = json.dumps(data, separators=(',',':'))
open(path, 'w').write(data)